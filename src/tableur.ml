open Array

(** Types *)

type resultat =
  | RVide
  | RChaine of string
  | REntier of int
  | RFlottant of float
  | RBooleen of bool
  | Erreur of erreur

and erreur = 
  | Mauvais_indice of (int * int)
  | Cycle_detecte
  | Argument_invalide
  | Division_par_zero

type expr =
  | Vide
  | Chaine of string
  | Entier of int
  | Flottant of float
  | Booleen of bool
  | Case of int * int
  | Unaire of op_unaire
  | Binaire of op_binaire
  | Reduction of op_reduction

and op_unaire = {app1 : resultat -> resultat; operande : expr}

and op_binaire = {app2 : resultat -> resultat -> resultat; gauche : expr; droite : expr}

and op_reduction = {app : resultat -> resultat -> resultat;
                    init : resultat;
                    case_debut : int * int;
                    case_fin : int * int}

type grille = expr array array


(** Fonctions *)

let cree_grille _i _j = make_matrix _i _j Vide

let cree_grille_result grid = make_matrix (length grid) (length grid.(0)) RVide

let est_dehors g = function
  | Case(i, j) -> (i >= length g) || (j >= length g.(i))
  | _ -> false

module ExprSet = Set.Make(struct
    type t = expr
    let compare = compare (* <=> let compare i j = compare i j *)
  end)

let rec eval_expr g e gr = 
  if (cycle g e) then
    Erreur(Cycle_detecte)
  else
    match e with
      | Vide -> RVide
      | Chaine(str) -> RChaine(str)
      | Entier(i) -> REntier(i)
      | Flottant(x) -> RFlottant(x)
      | Booleen(b) -> RBooleen(b)
      | Case(i, j) -> if (est_dehors g e) then
                        Erreur(Mauvais_indice(i, j))
                      else 
                        let res = eval_expr g g.(i).(j) gr in
                        gr.(i).(j) <- res;
                        res
      | Unaire(op) -> op.app1 (eval_expr g op.operande gr)
      | Binaire(op) -> op.app2 (eval_expr g op.gauche gr) (eval_expr g op.droite gr)
      | Reduction(op) -> List.fold_left op.app op.init (List.map (fun e -> eval_expr g e gr) (zone_to_list g op.case_debut op.case_fin))

and cycle g e = 
  let rec cycle_aux s e = 
    if (ExprSet.mem e s) then
      true
    else
      match e with
      | Case(i, j) -> if est_dehors g (Case(i, j)) then
                        (* Si la case pointe hors du tableur, on arrête la recherche de cycle, et on gèrera l'erreur par la suite *)
                        false
                      else 
                        cycle_aux (ExprSet.add e s) g.(i).(j)
      | Unaire(op) -> cycle_aux (ExprSet.add e s) op.operande
      | Binaire(op) -> (cycle_aux (ExprSet.add e s) op.gauche) || (cycle_aux (ExprSet.add e s) op.droite)
      | Reduction(op) -> (List.fold_left (fun x y -> x || y) 
                                        false 
                                        (List.map (fun ex -> cycle_aux (ExprSet.add e s) ex) (zone_to_list g op.case_debut op.case_fin)))
      | _ -> false
  in cycle_aux ExprSet.empty e

and zone_to_list g deb fin = 
  let (res : expr list ref) = ref [] in
  for i = (fst deb) to (fst fin) do
    for j = (snd deb) to (snd fin) do
      res := g.(i).(j) :: !res
    done
  done;
  !res

let eval_grille g = 
  let res = make_matrix (length g) (length g.(0)) RVide in
  for i = 0 to (length res) - 1 do
    for j = 0 to (length res.(i)) - 1 do
      if res.(i).(j) = RVide then
        res.(i).(j) <- eval_expr g g.(i).(j) res
    done
  done;
  res

let err_to_string = function
  | Mauvais_indice(_, _) -> "!Indice"
  | Cycle_detecte -> "!Cycle"
  | Argument_invalide -> "!Arg"
  | Division_par_zero -> "!Div"

let res_to_string = function
  | RVide -> " "
  | RChaine(str) -> str
  | REntier(n) -> string_of_int n
  | RFlottant(x) -> string_of_float x
  | RBooleen(b) -> string_of_bool b
  | Erreur(e) -> err_to_string e

let affiche_grille_resultat gr = 
  print_newline ();
  for i = 0 to (length gr) - 1 do
    for j = 0 to (length gr.(i)) - 1 do
      Format.printf "|%8s" (res_to_string gr.(i).(j));
    done;
    print_string "|";
    print_newline ();
  done

let rec expr_to_string = function
  | Vide -> " "
  | Chaine(str) -> str
  | Entier(n) -> string_of_int n
  | Flottant(x) -> string_of_float x
  | Booleen(b) -> string_of_bool b
  | Case(i, j) -> "@(" ^ (string_of_int i) ^ "," ^ (string_of_int j) ^ ")"
  | Unaire(op) -> "(fun1 " ^ (expr_to_string op.operande) ^ ")"
  | Binaire(op) -> "(fun2 " ^ (expr_to_string op.gauche) ^ " " ^ (expr_to_string op.droite) ^ ")"
  | Reduction(op) -> "(funN " ^ (res_to_string op.init) ^ " " 
                      ^ "(" ^ (string_of_int (fst op.case_debut)) ^ ", " ^ (string_of_int (snd op.case_debut)) ^ ")" ^ " " 
                      ^ "(" ^ (string_of_int (fst op.case_fin)) ^ ", " ^ (string_of_int (snd op.case_fin)) ^ ")" ^ ")"

let affiche_grille g = 
  print_newline ();
  for i = 0 to (length g) - 1 do
    for j = 0 to (length g.(i)) - 1 do
      Format.printf "|%8s" (expr_to_string g.(i).(j));
    done;
    print_string "|";
    print_newline ();
  done


(** Unaires *)
(* type : expr -> expr *)

(* Valeur absolue *)
let abs operande = 
  let app = function
    | REntier(n) -> if n > 0 then REntier(n) else REntier(-n)
    | RFlottant(x) -> if x > 0. then RFlottant(x) else RFlottant(-.x)
    | _ -> Erreur(Argument_invalide)
  in Unaire({app1 = app; operande = operande})

(* Oppose *)
let opp operande = 
  let app = function
    | REntier(n) -> REntier(-n)
    | RFlottant(x) -> RFlottant(-.x)
    | RBooleen(b) -> RBooleen(not b)
    | _ -> Erreur(Argument_invalide)
  in Unaire({app1 = app; operande = operande})

(* Inverse *)
let inv operande = 
  let app = function
    | REntier(n) -> if n = 0 then Erreur(Division_par_zero) else RFlottant(1. /. (float_of_int n))
    | RFlottant(x) -> if x = 0. then Erreur(Division_par_zero) else RFlottant(1. /. x)
    | RBooleen(b) -> RBooleen(not b)
    | _ -> Erreur(Argument_invalide)
  in Unaire({app1 = app; operande = operande})

(* Factorielle *)
let rec fact_ent = function
  | 0 -> 1
  | 1 -> 1
  | n -> n * fact_ent (n - 1)

let fact operande = 
  let app = function
    | REntier(n) -> REntier(fact_ent n)
    | _ -> Erreur(Argument_invalide)
  in Unaire({app1 = app; operande = operande})
  
(* Partie entière *)
let part_ent operande = 
  let app = function
    | REntier(n) -> REntier(n)
    | RFlottant(x) -> REntier(int_of_float (floor x))
    | _ -> Erreur(Argument_invalide)
  in Unaire({app1 = app; operande = operande})

(* Partie fractionnaire *)
let part_frac operande = 
  let app = function
    | REntier(_) -> RFlottant(0.)
    | RFlottant(x) -> RFlottant(x -. (floor x))
    | _ -> Erreur(Argument_invalide)
  in Unaire({app1 = app; operande = operande})

(* Plafond *)
let plafond operande = 
  let app = function
    | REntier(n) -> REntier(n)
    | RFlottant(x) -> REntier(int_of_float (ceil x))
    | _ -> Erreur(Argument_invalide)
  in Unaire({app1 = app; operande = operande})

(* Fibo *)
let rec fibo_aux acc1 acc2 = function
  | 0 -> acc1
  | 1 -> acc2
  | n -> fibo_aux acc2 (acc1 + acc2) (n - 1)

let fibo operande = 
  let app = function
    | REntier(n) -> REntier(fibo_aux 1 1 n)
    | _ -> Erreur(Argument_invalide)
  in Unaire({app1 = app; operande = operande})

(* Racine *)
let racine operande = 
  let app = function
    | REntier(n) -> RFlottant(sqrt (float_of_int n))
    | RFlottant(x) -> RFlottant(sqrt x)
    | _ -> Erreur(Argument_invalide)
  in Unaire({app1 = app; operande = operande})

(* Minuscules *)
let minuscules operande = 
  let app = function
    | RChaine(c) -> RChaine(String.lowercase_ascii c)
    | _ -> Erreur(Argument_invalide)
  in Unaire({app1 = app; operande = operande})

(* Majuscules *)
let majuscules operande = 
  let app = function
    | RChaine(c) -> RChaine(String.uppercase_ascii c)
    | _ -> Erreur(Argument_invalide)
  in Unaire({app1 = app; operande = operande})

(* Majuscule initiale *)
let maj_init operande = 
  let app = function
    | RChaine(c) -> RChaine(String.capitalize_ascii c)
    | _ -> Erreur(Argument_invalide)
  in Unaire({app1 = app; operande = operande})


(** Binaires *)
(* type : expr -> expr -> expr *)

(* Addition *)
let add_aux g d = 
    match g with
    | REntier(n) -> (match d with
                      | REntier(m) -> REntier(n + m)
                      | RFlottant(y) -> RFlottant((float_of_int n) +. y)
                      | _ -> Erreur(Argument_invalide))
    | RFlottant(x) -> (match d with
                        | REntier(m) -> RFlottant(x +. (float_of_int m))
                        | RFlottant(y) -> RFlottant(x +. y)
                        | _ -> Erreur(Argument_invalide))
    | RChaine(c) -> (match d with
                      | RChaine(s) -> RChaine(c ^ s)
                      | _ -> Erreur(Argument_invalide))
    | _ -> Erreur(Argument_invalide)

let add gauche droite = Binaire({app2 = add_aux; gauche = gauche; droite = droite})

(* Soustraction *)
let sub_aux g d = 
  match g with
  | REntier(n) -> (match d with
                    | REntier(m) -> REntier(n - m)
                    | RFlottant(y) -> RFlottant((float_of_int n) -. y)
                    | _ -> Erreur(Argument_invalide))
  | RFlottant(x) -> (match d with
                      | REntier(m) -> RFlottant(x -. (float_of_int m))
                      | RFlottant(y) -> RFlottant(x -. y)
                      | _ -> Erreur(Argument_invalide))
  | _ -> Erreur(Argument_invalide)

let sub gauche droite = Binaire({app2 = sub_aux; gauche = gauche; droite = droite})

(* Multiplication *)
let mult_chaine n c = 
  let res = ref "" in
  for _ = 1 to n do
    res := !res ^ c
  done;
  !res

let mult_aux g d = 
  match g with
  | REntier(n) -> (match d with
                    | REntier(m) -> REntier(n * m)
                    | RFlottant(y) -> RFlottant((float_of_int n) *. y)
                    | RChaine(c) -> RChaine(mult_chaine n c)
                    | _ -> Erreur(Argument_invalide))
  | RFlottant(x) -> (match d with
                      | REntier(m) -> RFlottant(x *. (float_of_int m))
                      | RFlottant(y) -> RFlottant(x *. y)
                      | _ -> Erreur(Argument_invalide))
  | RChaine(c) -> (match d with
                    | REntier(n) -> RChaine(mult_chaine n c)
                    | _ -> Erreur(Argument_invalide))
  | _ -> Erreur(Argument_invalide)

let mult gauche droite = Binaire({app2 = mult_aux; gauche = gauche; droite = droite})

(* Division *)
let div_aux g d = 
  match g with
  | REntier(n) -> (match d with
                    | REntier(m) -> if m = 0 then Erreur(Division_par_zero) else RFlottant((float_of_int n) /. (float_of_int m))
                    | RFlottant(y) -> if y = 0. then Erreur(Division_par_zero) else RFlottant((float_of_int n) /. y)
                    | _ -> Erreur(Argument_invalide))
  | RFlottant(x) -> (match d with
                      | REntier(m) -> if m = 0 then Erreur(Division_par_zero) else RFlottant(x /. (float_of_int m))
                      | RFlottant(y) -> if y = 0. then Erreur(Division_par_zero) else RFlottant(x /. y)
                      | _ -> Erreur(Argument_invalide))
  | _ -> Erreur(Argument_invalide)

let div gauche droite = Binaire({app2 = div_aux; gauche = gauche; droite = droite})

(* Puissance *)
let rec puiss_int a = function
  | 0 -> 1
  | 1 -> a
  | n -> let b = puiss_int a (n / 2) in
          b * b * (if n mod 2 = 0 then 1 else a)

let puiss_aux g d = 
  match g with
  | REntier(n) -> (match d with
                    | REntier(m) -> REntier(puiss_int n m)
                    | RFlottant(y) -> RFlottant((float_of_int n) ** y)
                    | _ -> Erreur(Argument_invalide))
  | RFlottant(x) -> (match d with
                      | REntier(m) -> RFlottant(x ** (float_of_int m))
                      | RFlottant(y) -> RFlottant(x ** y)
                      | _ -> Erreur(Argument_invalide))
  | _ -> Erreur(Argument_invalide)

let puiss gauche droite = Binaire({app2 = puiss_aux; gauche = gauche; droite = droite})

(* Minimum/maximum *)
let min_max_aux f g d = 
  match g with
  | REntier(n) -> (match d with
                    | REntier(m) -> REntier(int_of_float (f (float_of_int n) (float_of_int m)))
                    | RFlottant(y) -> if (f (float_of_int n) y) = y then RFlottant(y) else REntier(n)
                    | _ -> Erreur(Argument_invalide))
  | RFlottant(x) -> (match d with
                      | REntier(m) -> if (f x (float_of_int m)) = x then RFlottant(x) else REntier(m)
                      | RFlottant(y) -> RFlottant(f x y)
                      | _ -> Erreur(Argument_invalide))
  | _ -> Erreur(Argument_invalide)

let mini gauche droite = Binaire({app2 = (min_max_aux min); gauche = gauche; droite = droite})

let maxi gauche droite = Binaire({app2 = (min_max_aux max); gauche = gauche; droite = droite})


(** Reductions *)
(* type : (int * int) -> (int * int) -> expr *)

(* Correspondance avec fonctions binaires *)
let addN debut fin = Reduction({app = add_aux; init = REntier(0); case_debut = debut; case_fin = fin})
let multN debut fin = Reduction({app = mult_aux; init = REntier(1); case_debut = debut; case_fin = fin})
let miniN debut fin = Reduction({app = (min_max_aux min); init = REntier(0); case_debut = debut; case_fin = fin})
let maxiN debut fin = Reduction({app = (min_max_aux max); init = REntier(0); case_debut = debut; case_fin = fin})

(* Moyenne *)
(* On voit la moyenne comme une addition coefficientée par l'inverse du nombre d'éléments *)
let moy_aux c g d = 
  match g with
  | REntier(n) -> (match d with
                    | REntier(m) -> RFlottant((float_of_int n) +. c *. (float_of_int m))
                    | RFlottant(y) -> RFlottant((float_of_int n) +. c*. y)
                    | _ -> Erreur(Argument_invalide))
  | RFlottant(x) -> (match d with
                      | REntier(m) -> RFlottant(x +. c *. (float_of_int m))
                      | RFlottant(y) -> RFlottant(x +. c *. y)
                      | _ -> Erreur(Argument_invalide))
  | _ -> Erreur(Argument_invalide)

let moy debut fin = 
  let coef = 1. /. (float_of_int (((fst fin) - (fst debut) + 1) * ((snd fin) - (snd debut) + 1))) in
  Reduction({app = (moy_aux coef); init = REntier(0); case_debut = debut; case_fin = fin})

(* ecart-type *)
(* Faisable en O((n*m)^2) en recalculant la moyenne à chaque case ... Autre solution ? *)



(* Copié à partir du tableur.ml fourni pour la partie 2 *)
let int_to_letter j =
  let ltval = j mod 26 in
  let nb = (j - ltval) / 26 in
  let nb = if nb = 0 then "" else string_of_int nb in
  let lt = char_of_int (int_of_char 'A' + ltval) in
  Format.sprintf "%c%s" lt nb

module CaseSet = Set.Make (struct
  type t = int * int

  let compare = compare
end)

let ( -- ) i j = List.init (j - i + 1) (fun x -> x + i)

let rec product a b =
  match a with
  | [] ->
      []
  | hd :: tl ->
      List.map (fun x -> (hd, x)) b @ product tl b

let coords_of_plage (i, j) (i', j') =
  let l = if i < i' then i -- i' else i' -- i in
  let j = if j < j' then j -- j' else j' -- j in
  product l j