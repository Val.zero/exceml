open Tableur

let test1 () =
  let grille = cree_grille 10 10 in
  ignore grille

let test2 () =
  let grille = cree_grille 10 10 in
  grille.(0).(0) <- Entier 1 ;
  assert (grille.(0).(0) = Entier 1) ;
  ()

let test3 () =
  let grille = cree_grille 3 3 in
  grille.(0).(0) <- Vide;
  grille.(0).(2) <- Chaine("hello");
  grille.(1).(1) <- Entier(42);
  grille.(2).(0) <- Flottant(31.008);
  grille.(2).(2) <- Case(0, 2);
  grille.(0).(1) <- Unaire({app1 = (fun x -> x); operande = Case(1, 1)});
  grille.(1).(0) <- Binaire({app2 = (fun x y -> if x = RVide then y else x); gauche = Case(0, 0); droite = Case(0, 1)});
  grille.(2).(1) <- Reduction({app = (fun x y -> if x = RVide then y else x); init = RVide; case_debut = (0, 0); case_fin = (1, 2)});
  affiche_grille grille

let test4 () =
  let grille = cree_grille 2 2 in
  grille.(0).(0) <- Case(5, 5);
  grille.(0).(1) <- Case(1, 0);
  assert (est_dehors grille grille.(0).(0) = true);
  assert (est_dehors grille grille.(0).(1) = false);
  ()

let test5 () = 
  let grille = cree_grille 10 10 in
  grille.(0).(0) <- Case(0, 2);
  grille.(0).(2) <- Case(2, 2);
  grille.(2).(2) <- Case(2, 0);
  grille.(2).(0) <- Case(0, 0);

  grille.(3).(0) <- Case(0, 0);
  grille.(3).(1) <- Case(3, 0);

  grille.(4).(0) <- Unaire({app1 = (fun x -> x); operande = Case(4, 1)});
  grille.(4).(1) <- Case(4, 0);

  grille.(4).(2) <- Binaire({app2 = (fun x y -> if x = RVide then y else x); gauche = Vide; droite = Case(4, 3)});
  grille.(4).(3) <- Case(4, 2);

  grille.(1).(1) <- Reduction({app = (fun x y -> if x = RVide then y else x); init = RVide; case_debut = (0, 0); case_fin = (2, 2)});

  grille.(7).(7) <- Case(6, 6);
  grille.(6).(6) <- Case(5, 5);
  grille.(5).(5) <- Entier(1);
  
  assert (cycle grille grille.(0).(0) = true);
  assert (cycle grille grille.(3).(1) = true);
  assert (cycle grille grille.(4).(0) = true);
  assert (cycle grille grille.(4).(2) = true);
  assert (cycle grille grille.(1).(1) = true);
  assert (cycle grille grille.(3).(3) = false);
  ()

let test6 () = 
  let grille = cree_grille 3 3 in
  let grille_res = Array.make_matrix 3 3 RVide in
  grille.(0).(0) <- Case(0, 1);
  grille.(0).(1) <- Case(0, 0);
  grille.(0).(2) <- Case(5, 5);

  grille.(1).(0) <- Entier(42);
  grille.(1).(1) <- Case(1, 0);

  assert (eval_expr grille grille.(0).(0) grille_res = Erreur(Cycle_detecte));
  assert (eval_expr grille grille.(0).(2) grille_res = Erreur(Mauvais_indice(5, 5)));
  assert (eval_expr grille grille.(1).(0) grille_res = REntier(42));
  assert (eval_expr grille grille.(1).(1) grille_res = REntier(42));
  ()

let test7 () = 
  let grille = cree_grille 3 3 in
  let grille_res = Array.make_matrix 3 3 RVide in

  grille.(0).(0) <- Vide;
  grille.(0).(2) <- Chaine("hello");
  grille.(1).(1) <- Entier(42);
  grille.(2).(0) <- Flottant(31.008);
  grille.(2).(2) <- Case(0, 2);

  grille_res.(0).(0) <- RVide;
  grille_res.(0).(2) <- RChaine("hello");
  grille_res.(1).(1) <- REntier(42);
  grille_res.(2).(0) <- RFlottant(31.008);
  grille_res.(2).(2) <- RChaine("hello");

  assert (grille_res = eval_grille grille);
  ()

let test8 () = 
  let grille = cree_grille 3 3 in
  grille.(0).(0) <- Vide;
  grille.(0).(1) <- Case(0, 2);
  grille.(0).(2) <- Chaine("hello");
  grille.(1).(0) <- Case(1, 2);
  grille.(1).(1) <- Entier(42);
  grille.(1).(2) <- Flottant(31.008);
  grille.(2).(0) <- Unaire({app1 = (fun x -> x); operande = Case(1, 1)});
  grille.(2).(1) <- Binaire({app2 = (fun x y -> if x = RVide then y else x); gauche = Case(0, 0); droite = Case(0, 1)});
  grille.(2).(2) <- Reduction({app = (fun x y -> if x = RVide then y else x); init = RVide; case_debut = (1, 0); case_fin = (1, 2)});
  affiche_grille_resultat (eval_grille grille)

let test9 () = 
  let g = cree_grille 1 1 in
  let gr = Array.make_matrix 1 1 RVide in
  assert(eval_expr g (abs (Entier 3)) gr = REntier 3);
  assert(eval_expr g (abs (Entier (-3))) gr = REntier 3);

  assert(eval_expr g (opp (Entier 3)) gr = REntier (-3));
  assert(eval_expr g (opp (Entier (-3))) gr = REntier 3);
  assert(eval_expr g (opp (Booleen true)) gr = RBooleen false);

  assert(eval_expr g (inv (Entier 3)) gr = RFlottant (1./.3.));
  assert(eval_expr g (inv (Entier 0)) gr = Erreur(Division_par_zero));
  assert(eval_expr g (opp (Booleen false)) gr = RBooleen true);

  assert(eval_expr g (part_ent (Entier 3)) gr = REntier 3);
  assert(eval_expr g (part_ent (Flottant 3.25)) gr = REntier 3);

  assert(eval_expr g (part_frac (Entier 3)) gr = RFlottant 0.);
  assert(eval_expr g (part_frac (Flottant 3.25)) gr = RFlottant 0.25);

  assert(eval_expr g (plafond (Entier 3)) gr = REntier 3);
  assert(eval_expr g (plafond (Flottant 3.25)) gr = REntier 4);

  assert(eval_expr g (fact (Entier 5)) gr = REntier 120);
  assert(eval_expr g (fibo (Entier 5)) gr = REntier 8);

  assert(eval_expr g (racine (Entier 25)) gr = RFlottant 5.);
  assert(eval_expr g (racine (Flottant 25.)) gr = RFlottant 5.);

  assert(eval_expr g (minuscules (Chaine "Sorbonne Université")) gr = RChaine "sorbonne université");
  assert(eval_expr g (majuscules (Chaine "Sorbonne Université")) gr = RChaine "SORBONNE UNIVERSITé");
  assert(eval_expr g (maj_init (Chaine "Sorbonne Université")) gr = RChaine "Sorbonne Université");

  ()

let test10 () = 
  let g = cree_grille 1 1 in
  let gr = Array.make_matrix 1 1 RVide in
  assert(eval_expr g (add (Entier 3) (Entier 4)) gr = REntier 7);
  assert(eval_expr g (add (Entier 3) (Flottant 4.)) gr = RFlottant 7.);
  assert(eval_expr g (add (Flottant 3.) (Entier 4)) gr = RFlottant 7.);
  assert(eval_expr g (add (Flottant 3.) (Flottant 4.)) gr = RFlottant 7.);
  assert(eval_expr g (add (Chaine "Sorbonne") (Chaine "Université")) gr = RChaine "SorbonneUniversité");
  assert(eval_expr g (add (Booleen true) (Entier 4)) gr = Erreur(Argument_invalide));

  assert(eval_expr g (sub (Entier 3) (Entier 4)) gr = REntier (-1));
  assert(eval_expr g (sub (Entier 3) (Flottant 4.)) gr = RFlottant (-1.));
  assert(eval_expr g (sub (Flottant 3.) (Entier 4)) gr = RFlottant (-1.));
  assert(eval_expr g (sub (Flottant 3.) (Flottant 4.)) gr = RFlottant (-1.));

  assert(eval_expr g (mult (Entier 3) (Entier 4)) gr = REntier 12);
  assert(eval_expr g (mult (Entier 3) (Flottant 4.)) gr = RFlottant 12.);
  assert(eval_expr g (mult (Flottant 3.) (Entier 4)) gr = RFlottant 12.);
  assert(eval_expr g (mult (Flottant 3.) (Flottant 4.)) gr = RFlottant 12.);
  assert(eval_expr g (mult (Chaine "Sorbonne") (Entier 2)) gr = RChaine "SorbonneSorbonne");
  assert(eval_expr g (mult (Entier 2) (Chaine "Université")) gr = RChaine "UniversitéUniversité");

  assert(eval_expr g (div (Entier 3) (Entier 4)) gr = RFlottant (3. /. 4.));
  assert(eval_expr g (div (Entier 3) (Flottant 4.)) gr = RFlottant (3./.4.));
  assert(eval_expr g (div (Flottant 3.) (Entier 4)) gr = RFlottant (3./.4.));
  assert(eval_expr g (div (Flottant 3.) (Flottant 4.)) gr = RFlottant (3./.4.));
  assert(eval_expr g (div (Entier 3) (Entier 0)) gr = Erreur(Division_par_zero));

  assert(eval_expr g (puiss (Entier 3) (Entier 4)) gr = REntier 81);
  assert(eval_expr g (puiss (Entier 3) (Flottant 4.)) gr = RFlottant 81.);
  assert(eval_expr g (puiss (Flottant 3.) (Entier 4)) gr = RFlottant 81.);
  assert(eval_expr g (puiss (Flottant 3.) (Flottant 4.)) gr = RFlottant 81.);

  assert(eval_expr g (mini (Entier 3) (Entier 4)) gr = REntier 3);
  assert(eval_expr g (maxi (Entier 3) (Flottant 4.)) gr = RFlottant 4.);
  assert(eval_expr g (mini (Flottant 3.) (Entier 4)) gr = RFlottant 3.);
  assert(eval_expr g (maxi (Flottant 3.) (Flottant 4.)) gr = RFlottant 4.);

  ()

let test11 () = 
  let g = cree_grille 2 2 in
  let gr = Array.make_matrix 1 1 RVide in
  g.(0).(0) <- Entier(-42);
  g.(0).(1) <- Entier(28);
  g.(1).(0) <- Flottant(-2.5);
  g.(1).(1) <- Flottant(0.);

  assert(eval_expr g (addN (0, 0) (1, 1)) gr = RFlottant (-16.5));
  assert(eval_expr g (multN (0, 0) (1, 1)) gr = RFlottant 0.);
  assert(eval_expr g (miniN (0, 0) (1, 1)) gr = REntier (-42));
  assert(eval_expr g (maxiN (0, 0) (1, 1)) gr = REntier 28);
  assert(eval_expr g (moy (0, 0) (1, 1)) gr = RFlottant (-4.125));
  ()

let run_tests () =
  let liste_tests =
    [("création grille", test1); 
    ("affectation grille", test2); 
    ("affichage grille", test3);
    ("cases poitant à l'extérieur", test4);
    ("détection de cycles", test5);
    ("évaluation d'expressions", test6);
    ("évaluation de grille", test7);
    ("affichage grille résultat", test8);
    ("fonctions unaires", test9);
    ("fonctions binaires", test10);
    ("réductions", test11)]
  in
  List.iteri
    (fun i (nom_test, f_test) ->
      Format.printf "Test #%d - %s:\t" (i + 1) nom_test ;
      try
        f_test () ;
        Format.printf "\027[32mOk\n\027[39m"
      with exn ->
        Format.printf "\027[31mErreur - %s\n\027[39m" (Printexc.to_string exn))
    liste_tests

(* Main *)
let () = run_tests ()
