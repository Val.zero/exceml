open Js_utils

let range i j = List.init (j - i + 1) (fun x -> x + i)

type cell_infos = {
  container : Dom.div;
  inp : Dom.input;
  txt : Dom.txt;
  mutable result : Tableur.resultat;
  mutable parent_deps : ( int * int ) list;
  mutable child_deps : ( int * int ) list;
}

let direct_deps expr =
  let rec aux expr =
    let open Tableur in
    match expr with
    | Vide | Entier _ | Flottant _ | Chaine _ | Booleen _ ->
        CaseSet.empty
    | Case (i, j) ->
        CaseSet.singleton (i, j)
    | Unaire {operande; _} ->
        aux operande
    | Binaire {gauche; droite; _} ->
        CaseSet.union (aux gauche) (aux droite)
    | Reduction {case_debut; case_fin; _} ->
        List.fold_left (fun set c -> CaseSet.add c set) CaseSet.empty
        @@ coords_of_plage case_debut case_fin
  in
  Tableur.CaseSet.elements (aux expr)

type grid = Tableur.expr array array

type infos_grid = cell_infos array array

let mk_cell ?(inp = Dom.Create.input ()) ?(container = Dom.Create.div ())
    ?(txt = Dom.Create.txt " ") ?(result = Tableur.RVide) ?(parent_deps = []) 
    ?(child_deps = []) () =
  {inp; container; txt; result; parent_deps; child_deps}

(* TODO : à modifier *)
let error_to_string e = Tableur.err_to_string e

let resultat_to_string r = Tableur.res_to_string r

let update_display infos_grid i j r = 
  Dom.Text.set_content infos_grid.(i).(j).txt (resultat_to_string r);
  match r with 
  | Erreur(e) -> Dom.Class.add infos_grid.(i).(j).container "cell-error"
  | _ -> Dom.Class.remove infos_grid.(i).(j).container "cell-error"

let update_deps infos_grid i j expr = 
  let cell = infos_grid.(i).(j) in
  let ex_parents = cell.parent_deps in
  let new_parents = direct_deps expr in
  cell.parent_deps <- new_parents;
  (* On retire la cellule des enfants de ses ancien parents *)
  List.iter
    (fun coords -> let parent = infos_grid.(fst coords).(snd coords) in
                   parent.child_deps <- List.filter 
                                          (fun coords -> ((fst coords) != i) || ((snd coords) != j)) 
                                          parent.child_deps)
    ex_parents;
  (* On ajoute la cellule aux enfants de ses nouveaux parents *)
  List.iter
    (fun coords -> let parent = infos_grid.(fst coords).(snd coords) in
                  parent.child_deps <- (i, j)::parent.child_deps)
    new_parents

type cell_coords = {
  cell : string;
  i : int;
  j : int;
}

let grid_to_string grid infos_grid = 
  let cells_grid = Array.mapi 
    (fun i a -> Array.mapi (fun j c -> {cell = Dom.Input.get_value c.inp; i = i; j = j}) a) 
    infos_grid
  in
  let (cells_array:cell_coords array) = Array.concat (Array.to_list cells_grid) in
  Array.fold_left 
    (let cell_infos_to_string (s:string) c =
      if String.trim c.cell = "" then
        s
      else
        s ^ (string_of_int c.i) ^ "|" ^ (string_of_int c.j) ^ "|" ^ c.cell ^ "\n"
    in
    cell_infos_to_string
    )
    ""
    cells_array

let cells_of_string storage_grid = 
  let cells = String.split_on_char '\n' storage_grid in
  List.map 
    (fun s -> 
      let split = String.split_on_char '|' s in
      match split with 
      | i::j::c::_ -> Some ({cell = c; i = (int_of_string i); j = (int_of_string j)})
      | _ -> None)
    cells

let rec update i j grid infos_grid = 
  let cell = infos_grid.(i).(j) in
  Dom.Text.set_content cell.txt (Dom.Input.get_value cell.inp);
  Dom.Class.remove cell.inp "editing-input";
  match Parser.Ast.make (Dom.Input.get_value cell.inp) with
  | Ok (expr) -> grid.(i).(j) <- expr;
                 (* Note : la grille result servait de mémo; on ne l'utilise pas ici pour l'intant. *)
                 cell.result <- Tableur.eval_expr grid expr (Tableur.cree_grille_result grid);
                 update_deps infos_grid i j expr;
                 propagate grid infos_grid i j;
                 update_display infos_grid i j cell.result;
  | Error (_) -> ();
  Storage.set (grid_to_string grid infos_grid)

and propagate grid infos_grid i j = 
  let cell = infos_grid.(i).(j) in
  List.iter
    (fun coords -> update (fst coords) (snd coords) grid infos_grid)
    cell.child_deps

let add_cell_events i j grid infos_grid = 
  let cell = infos_grid.(i).(j) in
  let dblclick () = Dom.Class.add cell.inp "editing-input";
                    Dom.Focus.focus cell.inp in
  let blur () = update i j grid infos_grid in
  (* Note : La double condition assure une compatibilité avec le caractère de fin
  de ligne sous Windows ('\r') et sous Unix ('\n'). *)
  let keydown n = if Char.code '\r' == n || Char.code '\n' == n then 
                    Dom.Focus.blur cell.inp; 
                  true in
  Dom.Events.set_ondblclick cell.container dblclick;
  Dom.Events.set_onblur cell.inp blur;
  Dom.Events.set_onkeydown cell.inp keydown

let build_cell cells = 
  let cell = mk_cell () in
  Dom.appendChild cell.container cell.inp;
  Dom.appendChild cell.container cell.txt;
  Dom.Class.add cell.container "cell-container";
  Dom.appendChild cells cell.container;
  cell 

let load_grids height width =
  let cells = Dom.get_element_by_id "cells" in
  Init.set_grid_template cells height width ;
  let lines = range 0 (height - 1) in
  let columns = range 0 (width - 1) in
  Init.build_headers cells lines columns ;
  let grid = Array.make_matrix height width Tableur.Vide in
  let infos_grid =
    Array.init height @@ fun _ -> Array.init width @@ fun _ -> build_cell cells
  in
  (grid, infos_grid)

let load_storage grid infos_grid = match Storage.find () with
  | None -> ()
  | Some (s) -> let cells = cells_of_string s in
                List.iter 
                  (fun o -> match o with 
                            | Some (c) -> Dom.Input.set_value infos_grid.(c.i).(c.j).inp c.cell;
                                          Dom.Text.set_content infos_grid.(c.i).(c.j).txt c.cell;
                                          update c.i c.j grid infos_grid
                            | None -> ())
                  cells

let main () =
  let height = 10 in
  let width = 10 in
  let (grid, infos_grid) = load_grids height width in
  let () = load_storage grid infos_grid in
  Array.iteri
    (fun i a -> Array.iteri (fun j c -> add_cell_events i j grid infos_grid) a)
    grid

let () = Init.onload main
